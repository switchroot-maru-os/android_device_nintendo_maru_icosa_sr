# Inherit some common lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_tablet_wifionly.mk)

# Inherit device configuration for icosa.
$(call inherit-product, device/nvidia/foster/lineage.mk)
$(call inherit-product, device/nintendo/maru_icosa_sr/full_maru_icosa_sr.mk)

# Include Maru stuff
$(call inherit-product, vendor/maruos/device-maru.mk)
$(call inherit-product, vendor/maruos/BoardConfigVendor.mk)

TARGET_INIT_VENDOR_LIB := //device/nintendo/icosa_sr:libinit_icosa_sr

PRODUCT_NAME := maru_icosa_sr
PRODUCT_DEVICE := icosa_sr
